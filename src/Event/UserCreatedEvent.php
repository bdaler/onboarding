<?php
/**
 * Created by PhpStorm.
 * User: bdaler
 * Date: 2019-09-29
 * Time: 15:15
 */

namespace App\Event;

use App\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserCreatedEvent extends Event
{

    public const EVENT_TYPE = 'user.created';

    /**
     * @var User
     */
    protected $user;

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return UserCreatedEvent
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
}