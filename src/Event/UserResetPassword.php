<?php
/**
 * Created by PhpStorm.
 * User: bdaler
 * Date: 2019-10-03
 * Time: 12:14
 */

namespace App\Event;


use App\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserResetPassword extends Event
{
    public const EVENT_TYPE = 'user.reset.password';
    /**
     * @var User
     */
    private $user;

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return UserResetPassword
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

}