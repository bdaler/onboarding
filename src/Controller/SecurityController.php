<?php

namespace App\Controller;


use App\Auth\Dto\Request\ConfirmResetPasswordRequest;
use App\Auth\Dto\Request\ResetPasswordRequest;
use App\Auth\Dto\Request\UserRegisterRequest;
use App\Auth\Entity\ResetPassword;
use App\Auth\Security;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class SecurityController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    /**
     * SecurityController constructor.
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/login", name="login", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $user = $this->getUser();
        return $this->json(['username' => $user->getUserName(), 'roles' => $user->getRoles()]);
    }

    /**
     * Регистрация пользователя.
     *
     * @Route("/register", name="register", methods={"POST"})
     *
     * @ParamConverter(
     *     "userRegisterRequest",
     *     converter="fos_rest.request_body",
     *     class="App\Auth\Dto\Request\UserRegisterRequest"
     * )
     *
     * @param UserRegisterRequest $userRegisterRequest
     * @param ConstraintViolationListInterface $validationErrors
     * @return View
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function register(UserRegisterRequest $userRegisterRequest, ConstraintViolationListInterface $validationErrors): View
    {
        if (count($validationErrors) > 0) {
            return View::create($validationErrors, Response::HTTP_BAD_REQUEST);
        }
        return View::create($this->security->registration($userRegisterRequest));
    }

    /**
     * @Route("/registration/confirm/{uuid}", name="confirm", methods={"POST"})
     * @param string $uuid
     * @return View
     */
    public function confirm(string $uuid): View
    {
        return View::create($this->security->confirm($uuid));
    }

    /**
     * @Route("/reset_password", name="resset_password", methods={"POST"})
     *
     * @ParamConverter(
     *     "resetPasswordRequest",
     *     converter="fos_rest.request_body",
     *     class="App\Auth\Dto\Request\ResetPasswordRequest"
     * )
     *
     * @param ResetPasswordRequest $resetPasswordRequest
     * @return View
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function resetPassword(ResetPasswordRequest $resetPasswordRequest): View
    {
        return View::create($this->security->resetPassword($resetPasswordRequest));
    }

    /**
     * @Route("/confirm_reset_password", name="confirm_reset_password", methods={"POST"})
     *
     * @ParamConverter(
     *     "confirmResetPasswordRequest",
     *     converter="fos_rest.request_body",
     *     class="App\Auth\Dto\Request\ConfirmResetPasswordRequest"
     * )
     *
     * @param ConfirmResetPasswordRequest $confirmResetPasswordRequest
     * @param ConstraintViolationListInterface $validationErrors
     * @return View
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function confirmResetPassword(ConfirmResetPasswordRequest $confirmResetPasswordRequest, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            return View::create($validationErrors, Response::HTTP_BAD_REQUEST);
        }
        return View::create($this->security->confirmResetPassword($confirmResetPasswordRequest));
    }
}
