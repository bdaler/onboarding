<?php
/**
 * Created by PhpStorm.
 * User: bdaler
 * Date: 2019-09-28
 * Time: 18:23
 */

namespace App\Response;

use stdClass;
use Symfony\Component\HttpFoundation\JsonResponse as SymfonyJsonResponse;

class JsonResponse extends SymfonyJsonResponse
{

    protected $content = [
        'data' => [],
        'errors' => []
    ];

    /**
     * @param $code
     * @param string $text
     * @return $this
     */
    public function addError($code, $text = ''): self
    {
        $this->content['errors'][$code]['description'] = ApiResponseStatus::$status[$code];
        if (!empty($text)) {
            $this->content['errors'][$code]['description'] .= $text;
        }
        return $this;
    }

    /**
     * Добавляем дополнительную посняющую ошибку
     * @param integer $parentErrorCode
     * @param integer $subErrorCode
     * @param string $subErrorDescription
     */
    public function addSubError(int $parentErrorCode, int $subErrorCode, string $subErrorDescription)
    {
        if (!isset($this->content['errors'][$parentErrorCode]['info'])) {
            $this->content['errors'][$parentErrorCode]['info'] = new stdClass();
        }
        $this->content['errors'][$parentErrorCode]['info']->{$subErrorCode} = $subErrorDescription;
    }

    public function sendContent()
    {
        echo $this->getContent();
        return $this;
    }

    /**
     * @return false|string|null
     */
    public function getContent()
    {
        if ($this->hasErrors()) {
            return json_encode(['errors' => $this->content['errors']]);
        }
        return $this->content['data'] ?: null;
    }

    public function setContent($data)
    {
        $this->content['data'] = $data;
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return (bool)$this->content['errors'];
    }

    /**
     * Sets the data to be sent as json.
     *
     * @param mixed $data
     *
     * @return JsonResponse
     */
    public function setData($data = array())
    {
        if (version_compare(PHP_VERSION, '5.5.0', '>=')) {
            $this->data = json_encode($data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT |
                JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_ERROR_RECURSION |
                JSON_ERROR_INF_OR_NAN | JSON_ERROR_UNSUPPORTED_TYPE | JSON_PRETTY_PRINT);
        } else {
            $this->data = json_encode($data,
                JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_PRETTY_PRINT);
        }
        return $this->update();
    }
}