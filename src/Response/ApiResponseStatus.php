<?php
/**
 * Created by PhpStorm.
 * User: bdaler
 * Date: 2019-09-28
 * Time: 17:59
 */

namespace App\Response;
class ApiResponseStatus
{

    public const SUCCESS = 'OK';
    public const ERROR = 'ERROR';

    public const USER_EXIST_CODE = 1;
    public const EMPTY_REQUIRED_FIELDS = 2;
    public const PASSWORDS_MUST_BE_EQUAL = 3;
    public const WRONG_ACTIVATION_CODE = 4;
    public const ACCOUNT_ALREADY_ACTIVATED = 5;
    public const ACCOUNT_ACTIVATED_SUCCESSFUL = 6;
    public const USER_NOT_FOUND = 7;
    public const EXPIRED_TOKEN_TIME = 8;
    public const PASSWORD_CHANGED_SUCCESSFUL = 9;
    public const INVALID_TOKEN = 10;

    public static $status = [
        self::USER_EXIST_CODE => 'user already exist',
        self::EMPTY_REQUIRED_FIELDS => 'required fields empty',
        self::PASSWORDS_MUST_BE_EQUAL => 'password and confirm_password must be equal',
        self::WRONG_ACTIVATION_CODE => 'activation code is empty or is wrong.',
        self::ACCOUNT_ALREADY_ACTIVATED => 'account already activated',
        self::ACCOUNT_ACTIVATED_SUCCESSFUL => 'You account successful activated',
        self::USER_NOT_FOUND => 'user with this email does\'t exist',
        self::EXPIRED_TOKEN_TIME => 'expired token',
        self::PASSWORD_CHANGED_SUCCESSFUL => 'password changed!',
        self::INVALID_TOKEN => 'invalid confirm token',
    ];

}