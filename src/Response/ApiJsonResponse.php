<?php
/**
 * Created by PhpStorm.
 * User: bdaler
 * Date: 2019-09-28
 * Time: 18:28
 */

namespace App\Response;


trait ApiJsonResponse
{
    /**
     * @param int $statusCode
     * @param array $data
     * @param array $header
     * @return JsonResponse
     */
    public function createResponse($statusCode = JsonResponse::HTTP_OK, $data = [], $header = []): JsonResponse
    {
        return new JsonResponse($data, $statusCode, $header);
    }
}