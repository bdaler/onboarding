<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 20.05.2020
 * Time: 14:25
 */

namespace App\Exception;


use Exception;

class DomainException extends \DomainException
{
    protected const DEFAULT_MESSAGE = '';
    protected const DEFAULT_STATUS_CODE = 0;

    /**
     * DomainException constructor.
     * @param null $message
     * @param null $code
     * @param Exception|null $previous
     */
    public function __construct($message = null, $code = null, Exception $previous = null)
    {
        $message = $message ?? static::DEFAULT_MESSAGE;
        $code = $code ?? static::DEFAULT_STATUS_CODE;

        parent::__construct($message, $code, $previous);
    }
}