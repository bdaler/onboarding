<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 20.05.2020
 * Time: 14:51
 */

namespace App\Exception;


class WithErrorCodeException extends DomainException
{
    protected const DEFAULT_ERROR_CODE = 0;

    /**
     * @var integer
     */
    private $errorCode;

    /**
     * WithErrorCodeException constructor.
     * @param null $message
     * @param int|null $errorCode
     * @param null $code
     * @param \Exception|null $previous
     */
    public function __construct($message = null, int $errorCode = null, $code = null, \Exception $previous = null)
    {
        $this->errorCode = $errorCode ?? static::DEFAULT_ERROR_CODE;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return int
     */
    public function getErrorCode(): int
    {
        return $this->errorCode;
    }
}