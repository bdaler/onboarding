<?php
/**
 * Created by PhpStorm.
 * User: bdaler
 * Date: 2019-10-05
 * Time: 14:31
 */

namespace App\Auth\Validator;


class ResetPassword implements AuthValidatorInterface
{
    /**
     * @var string
     */
    private $tokenExpireTime;

    /**
     * AuthValidator constructor.
     * @param string $tokenExpireTime
     */
    public function __construct(string $tokenExpireTime)
    {
        $this->tokenExpireTime = $tokenExpireTime;
    }

    /**
     * @param string $password
     * @param string $confirmPassword
     * @return bool
     */
    public function checkPasswords(string $password, string $confirmPassword): bool
    {
        return $password === $confirmPassword;
    }

    /**
     * @param \DateTime $createdAt
     * @return bool
     * @throws \Exception
     */
    public function isTokenExpired(\DateTime $createdAt): bool
    {
        $diff = (new \DateTime())->diff($createdAt)->s;
        return $diff >= $this->tokenExpireTime;
    }

    /**
     * @param array $fields
     * @return bool
     */
    public function checkRequiredFields(array $fields): bool
    {
        // TODO: Implement checkRequiredFields() method.
    }
}