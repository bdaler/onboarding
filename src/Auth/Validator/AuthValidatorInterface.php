<?php
/**
 * Created by PhpStorm.
 * User: bdaler
 * Date: 2019-10-05
 * Time: 14:28
 */

namespace App\Auth\Validator;


interface AuthValidatorInterface
{
    /**
     * @param string $password
     * @param string $confirmPassword
     * @return bool
     */
    public function checkPasswords(string $password, string $confirmPassword): bool;

    /**
     * @param array $fields
     * @return bool
     */
    public function checkRequiredFields(array $fields): bool;
}