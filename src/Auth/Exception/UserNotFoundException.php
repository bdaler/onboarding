<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 21.05.2020
 * Time: 12:26
 */

namespace App\Auth\Exception;


use App\Exception\WithErrorCodeException;
use App\Response\ApiResponseStatus;

class UserNotFoundException extends WithErrorCodeException
{
    protected const DEFAULT_STATUS_CODE = 409;
    protected const DEFAULT_ERROR_CODE = ApiResponseStatus::USER_NOT_FOUND;
    protected const DEFAULT_MESSAGE = 'user with this email does\'t exist';
}