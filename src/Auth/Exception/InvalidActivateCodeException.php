<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 20.05.2020
 * Time: 15:50
 */

namespace App\Auth\Exception;


use App\Exception\WithErrorCodeException;
use App\Response\ApiResponseStatus;

class InvalidActivateCodeException extends WithErrorCodeException
{
    protected const DEFAULT_STATUS_CODE = 400;
    protected const DEFAULT_ERROR_CODE = ApiResponseStatus::WRONG_ACTIVATION_CODE;
    protected const DEFAULT_MESSAGE = 'activation code is empty or is wrong.';
}