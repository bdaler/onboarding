<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 20.05.2020
 * Time: 14:47
 */

namespace App\Auth\Exception;


use App\Exception\WithErrorCodeException;
use App\Response\ApiResponseStatus;

class NotEqualPasswordException extends WithErrorCodeException
{
    protected const DEFAULT_STATUS_CODE = 409;
    protected const DEFAULT_ERROR_CODE = ApiResponseStatus::PASSWORDS_MUST_BE_EQUAL;
    protected const DEFAULT_MESSAGE = 'password and confirm_password must be equal';
}