<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 20.05.2020
 * Time: 15:48
 */

namespace App\Auth\Exception;


use App\Exception\WithErrorCodeException;
use App\Response\ApiResponseStatus;

class UserAlreadyActivatedException extends WithErrorCodeException
{
    protected const DEFAULT_STATUS_CODE = 400;
    protected const DEFAULT_ERROR_CODE = ApiResponseStatus::ACCOUNT_ALREADY_ACTIVATED;
    protected const DEFAULT_MESSAGE = 'account already activated';
}