<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 21.05.2020
 * Time: 13:15
 */

namespace App\Auth\Exception;


use App\Exception\WithErrorCodeException;
use App\Response\ApiResponseStatus;

class ExpiredTokenException  extends WithErrorCodeException
{
    protected const DEFAULT_STATUS_CODE = 409;
    protected const DEFAULT_ERROR_CODE = ApiResponseStatus::EXPIRED_TOKEN_TIME;
    protected const DEFAULT_MESSAGE = 'expired token';
}