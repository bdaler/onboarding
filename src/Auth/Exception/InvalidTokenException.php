<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 20.05.2020
 * Time: 15:50
 */

namespace App\Auth\Exception;


use App\Exception\WithErrorCodeException;
use App\Response\ApiResponseStatus;

class InvalidTokenException extends WithErrorCodeException
{
    protected const DEFAULT_STATUS_CODE = 400;
    protected const DEFAULT_ERROR_CODE = ApiResponseStatus::INVALID_TOKEN;
    protected const DEFAULT_MESSAGE = 'invalid confirm token';
}