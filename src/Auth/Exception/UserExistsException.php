<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 20.05.2020
 * Time: 14:17
 */

namespace App\Auth\Exception;


use App\Exception\WithErrorCodeException;
use App\Response\ApiResponseStatus;

class UserExistsException extends WithErrorCodeException
{
    protected const DEFAULT_STATUS_CODE = 409;
    protected const DEFAULT_ERROR_CODE = ApiResponseStatus::USER_EXIST_CODE;
    protected const DEFAULT_MESSAGE = 'user already exist';
}