<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 21.05.2020
 * Time: 11:42
 */

namespace App\Auth\Dto\Request;


class ConfirmResetPasswordRequest
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $token;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $confirmPassword;

}