<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 19.05.2020
 * Time: 14:58
 */

namespace App\Auth\Dto\Request;


/**
 * Class UserRegister
 * @package App\Auth\Dto\Request
 */
class UserRegisterRequest
{
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $password;
    /**
     * @var string
     */
    public $confirmPassword;
}