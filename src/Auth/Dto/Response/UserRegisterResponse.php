<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 19.05.2020
 * Time: 14:58
 */

namespace App\Auth\Dto\Response;


/**
 * Class UserRegister
 * @package App\Auth\Dto\Request
 */
class UserRegisterResponse
{
    /**
     * @var string
     */
    public $email;
    /**
     * @var array
     */
    public $roles;
}