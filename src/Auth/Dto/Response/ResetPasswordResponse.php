<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 21.05.2020
 * Time: 12:10
 */

namespace App\Auth\Dto\Response;


/**
 * Class ResetPasswordResponse
 * @package App\Auth\Dto\Response
 */
class ResetPasswordResponse
{
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $token;
}