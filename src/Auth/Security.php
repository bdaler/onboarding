<?php
/**
 * Created by PhpStorm.
 * User: bdaler
 * Date: 2019-09-28
 * Time: 17:11
 */

namespace App\Auth;


use App\Auth\Dto\Request\ConfirmResetPasswordRequest;
use App\Auth\Dto\Request\ResetPasswordRequest;
use App\Auth\Dto\Response\ResetPasswordResponse;
use App\Auth\Dto\Response\UserRegisterResponse;
use App\Auth\Entity\ResetPassword;
use App\Auth\Exception\ExpiredTokenException;
use App\Auth\Exception\InvalidTokenException;
use App\Auth\Exception\UserNotFoundException;
use App\Auth\Validator\AuthValidatorInterface;
use App\Auth\Dto\Request\UserRegisterRequest;
use App\Auth\Exception\InvalidActivateCodeException;
use App\Auth\Exception\NotEqualPasswordException;
use App\Auth\Exception\UserAlreadyActivatedException;
use App\Auth\Exception\UserExistsException;
use App\Entity\User;
use App\Event\UserCreatedEvent;
use App\Event\UserResetPassword;
use App\Response\JsonResponse;
use Doctrine\ORM\EntityManager;
use App\Response\ApiJsonResponse;
use App\Response\ApiResponseStatus;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class Security
{
    use ApiJsonResponse;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var UserCreatedEvent
     */
    private $userCreatedEvent;
    /**
     * @var AuthValidatorInterface
     */
    private $validator;
    /**
     * @var UserResetPassword
     */
    private $resetPasswordEvent;

    /**
     * Security constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $encoder
     * @param EventDispatcherInterface $eventDispatcher
     * @param UserCreatedEvent $userCreatedEvent
     * @param AuthValidatorInterface $validator
     * @param UserResetPassword $resetPasswordEvent
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $encoder,
        EventDispatcherInterface $eventDispatcher,
        UserCreatedEvent $userCreatedEvent,
        AuthValidatorInterface $validator,
        UserResetPassword $resetPasswordEvent
    )
    {
        $this->encoder = $encoder;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->userCreatedEvent = $userCreatedEvent;
        $this->validator = $validator;
        $this->resetPasswordEvent = $resetPasswordEvent;
    }

    /**
     * @param UserRegisterRequest $register
     * @return UserRegisterResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function registration(UserRegisterRequest $register): UserRegisterResponse
    {
        if ($register->password !== $register->confirmPassword) {
            throw new NotEqualPasswordException();
        }

        $userRepository = $this->entityManager->getRepository(User::class);
        $user = $userRepository->findOneByEmail($register->email);

        if ($user instanceof User) {
            throw new UserExistsException();
        }

        $user = new User();
        $user
            ->setEmail($register->email)
            ->setPassword($this->encoder->encodePassword($user, trim($register->password)))
            ->setRoles([UserRoleType::ROLE_USER])
            ->activate(false)
            ->setActivationCode(Uuid::uuid4());

        $userEvent = $this->userCreatedEvent->setUser($user);
        $this->eventDispatcher->dispatch($userEvent, UserCreatedEvent::EVENT_TYPE);

        $this->entityManager->persist($user);
        $this->entityManager->flush($user);

        $response = new UserRegisterResponse();
        $response->email = $user->getEmail();
        $response->roles = $user->getRoles();

        return $response;
    }

    public function confirm(string $uuid)
    {
        if (empty($uuid) && !Uuid::isValid($uuid)) {
            throw new InvalidActivateCodeException();
        }
        $user = $this->fetchUserByActivationCode($uuid);
        if ($user instanceof User) {
            if ($user->isActivated()) {
                throw new UserAlreadyActivatedException();
            }
            $user->activate(true);
            $this->entityManager->persist($user);
            $this->entityManager->flush($user);
        }
    }

    /**
     * @param string $uuid
     * @return mixed
     */
    public function fetchUserByActivationCode(string $uuid)
    {
        $userRepo = $this->entityManager->getRepository(User::class);
        return $userRepo->findOneByActivationCode($uuid);
    }

    /**
     * @param ResetPasswordRequest $request
     * @return ResetPasswordResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function resetPassword(ResetPasswordRequest $request): ResetPasswordResponse
    {
        $user = $this->fetchUserByEmail($request->email);

        if (!$user instanceof User) {
            throw new UserNotFoundException();
        }

        $user
            ->setResetPasswordToken(Uuid::uuid4())
            ->setTokenCreatedAt(new \DateTime());

        $this->entityManager->flush($user);

        $event = $this->resetPasswordEvent->setUser($user);
        $this->eventDispatcher->dispatch($event, UserResetPassword::EVENT_TYPE);

        $response = new ResetPasswordResponse();
        $response->email = $user->getEmail();
        $response->token = $user->getResetPasswordToken();

        return $response;
    }

    /**
     * @param string $email
     * @return mixed
     */
    private function fetchUserByEmail(string $email)
    {
        $userRepository = $this->entityManager->getRepository(User::class);
        return $userRepository->findOneByEmail($email);
    }

    /**
     * @param ConfirmResetPasswordRequest $confirmResetPasswordRequest
     * @return void
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function confirmResetPassword(ConfirmResetPasswordRequest $confirmResetPasswordRequest)
    {
        if (!Uuid::isValid($confirmResetPasswordRequest->token)) {
            throw new InvalidTokenException();
        }

        $user = $this->fetchUserByToken($confirmResetPasswordRequest);
        if (!$user instanceof User) {
            throw new UserNotFoundException();
        }

        if ($this->validator->isTokenExpired($user->getTokenCreatedAt())) {
            throw new ExpiredTokenException();
        }

        if (
            $confirmResetPasswordRequest->password !== $confirmResetPasswordRequest->confirmPassword
        ) {
            throw new NotEqualPasswordException();
        }

        $user->setPassword(
            $this->encodePassword($confirmResetPasswordRequest->password, $user)
        );
        $this->entityManager->persist($user);
        $this->entityManager->flush($user);
    }

    /**
     * @param ConfirmResetPasswordRequest $request
     * @return mixed
     */
    private function fetchUserByToken(ConfirmResetPasswordRequest $request)
    {
        $userRepo = $this->entityManager->getRepository(User::class);
        return $userRepo->findForResetPassword($request->email, $request->token);
    }

    /**
     * @param string $password
     * @param User $user
     * @return string
     */
    private function encodePassword(string $password, User $user): string
    {
        return $this->encoder->encodePassword($user, trim($password));
    }
}