<?php
/**
 * Created by PhpStorm.
 * User: bdaler
 * Date: 2019-10-03
 * Time: 12:21
 */

namespace App\Listener;


use App\Event\UserResetPassword;
use App\Service\Mailer;

/**
 * Class UserResetPasswordListener
 * @package App\Listener
 */
class UserResetPasswordListener
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * UserResetPasswordListener constructor.
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param UserResetPassword $event
     */
    public function onUserResetPassword(UserResetPassword $event): void
    {
        $this->mailer->sendResetPasswordMail($event->getUser());
    }
}