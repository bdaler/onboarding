<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 20.05.2020
 * Time: 14:31
 */

namespace App\Listener;


use App\Exception\DomainException;
use App\Exception\WithErrorCodeException;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UnexpectedValueException;

class ExceptionListener
{
    private const NOT_FOUND = 'Not found';
    private const INTERNAL_ERROR = 'Internal error';

    /**
     * @var string
     */
    private $env;

    /**
     * ExceptionListener constructor.
     * @param string $env
     */
    public function __construct(string $env)
    {
        $this->env = $env;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     * @throws UnexpectedValueException
     * @throws InvalidArgumentException
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $response = new Response();
        if ($exception instanceof NotFoundHttpException) {
            $message = $this->env === 'dev' ? $exception->getMessage() : self::NOT_FOUND;
            $this->setMessageResponse($response, $message);
            $response->setStatusCode($exception->getStatusCode());
        } elseif ($exception instanceof HttpExceptionInterface) {
            $this->setMessageResponse($response, $exception->getMessage());
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        } elseif ($exception instanceof WithErrorCodeException) {
            $this->setMessageResponse($response, $exception->getMessage(), $exception->getErrorCode());
            $response->setStatusCode($exception->getCode());
        } elseif ($exception instanceof DomainException) {
            $this->setMessageResponse($response, $exception->getMessage());
            $response->setStatusCode($exception->getCode());
        } else {
            $message = $this->env !== 'prod' ? (string)$exception : self::INTERNAL_ERROR;
            $this->setMessageResponse($response, $message);
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $event->setResponse($response);
    }

    /**
     * @param Response $response
     * @param $message
     * @param int $errorCode
     */
    private function setMessageResponse(Response $response, $message, $errorCode = 0): void
    {
        $response->setContent(json_encode(array_filter(['message' => $message, 'code' => $errorCode])));
    }
}