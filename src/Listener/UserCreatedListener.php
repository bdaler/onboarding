<?php
/**
 * Created by PhpStorm.
 * User: bdaler
 * Date: 2019-09-29
 * Time: 15:30
 */

namespace App\Listener;

use App\Auth\UserRoleType;
use App\Event\UserCreatedEvent;
use App\Service\Mailer;

/**
 * Class UserCreatedListener
 * @package App\Listener
 */
class UserCreatedListener
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * UserCreatedListener constructor.
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param UserCreatedEvent $event
     */
    public function onUserCreated(UserCreatedEvent $event): void
    {
        $event->getUser()->setRoles([UserRoleType::ROLE_USER]);
        $this->mailer->sendWelcomeMail($event->getUser());
    }
}