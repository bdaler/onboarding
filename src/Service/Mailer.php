<?php
/**
 * Created by PhpStorm.
 * User: bdaler
 * Date: 2019-09-29
 * Time: 20:50
 */

namespace App\Service;

use App\Entity\User;
use Swift_Mailer;
use Swift_Message;

class Mailer
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;
    /**
     * @var string
     */
    private $adminEmail;

    /**
     * Mailer constructor.
     * @param Swift_Mailer $mailer
     * @param string $adminEmail
     */
    public function __construct(Swift_Mailer $mailer, $adminEmail)
    {
        $this->mailer = $mailer;
        $this->adminEmail = $adminEmail;
    }

    /**
     * @param User $user
     */
    public function sendWelcomeMail(User $user): void
    {
        $message = (new Swift_Message('Welcome to our portal'))
            ->setFrom($this->adminEmail)
            ->setTo($user->getEmail())
            //TODO: вынести куда нить.
            ->setBody("Hello, please activate your account: http://onboarding.local:88/registration/confirm/{$user->getActivationCode()}");
        $this->mailer->send($message);
    }

    /**
     * @param User $user
     */
    public function sendResetPasswordMail(User $user): void
    {
        $message = (new \Swift_Message('Requested resetpassword'))
            ->setFrom($this->adminEmail)
            ->setTo($user->getEmail())
            ->setBody("Hello. your token to reset password is: {$user->getResetPasswordToken()}")
        ;
        $this->mailer->send($message);
    }
}