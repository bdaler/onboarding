<?php
/**
 * @author Daler Bahritdinov <dbahritdinov@htc-cs.ru>
 * Date: 20.05.2020
 * Time: 16:26
 */

namespace App\Tests\Auth\Service;


use App\Auth\Dto\Request\ResetPasswordRequest;
use App\Auth\Dto\Request\UserRegisterRequest;
use App\Auth\Dto\Response\ResetPasswordResponse;
use App\Auth\Dto\Response\UserRegisterResponse;
use App\Auth\Exception\UserAlreadyActivatedException;
use App\Auth\Exception\UserExistsException;
use App\Auth\Security;
use App\Auth\UserRoleType;
use App\Entity\User;
use App\Response\ApiResponseStatus;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SecurityTest extends KernelTestCase
{
    /**
     * @var Security
     */
    private $securityService;
    /**
     * @var \Doctrine\ORM\EntityManager|object|null
     */
    private $em;
    /**
     * @var UserRegisterRequest
     */
    private $registrationRequest;

    protected function setUp()
    {
        parent::setUp();
        $this->securityService = $this->bootKernel()->getContainer()->get(Security::class);
        $entityManager = $this->bootKernel()->getContainer()->get('doctrine.orm.entity_manager');
        $this->em = $entityManager;
        $purger = new ORMPurger($entityManager);
        $purger->purge();


        $userRegistrationRequest = new UserRegisterRequest();
        $userRegistrationRequest->email = 'test@tes.ru';
        $userRegistrationRequest->password = 'strongPassword';
        $userRegistrationRequest->confirmPassword = 'strongPassword';
        $this->registrationRequest = $userRegistrationRequest;
    }

    public function testRegistration()
    {
        /** @var Security $secureService */
        $response = $this->securityService->registration($this->registrationRequest);

        $this->assertInstanceOf(UserRegisterResponse::class, $response);
        $this->assertEquals($this->registrationRequest->email, $response->email);
        $this->assertEquals(UserRoleType::ROLE_USER, $response->roles[0]);

        $user = $this->em->getRepository(User::class)->findOneByEmail($this->registrationRequest->email);
        $this->assertFalse($user->isActivated());
        $this->assertNotEmpty($user->getActivationCode());
    }

    public function testUserExistsException()
    {
        $user = new User();
        $user
            ->setEmail($this->registrationRequest->email)
            ->setPassword($this->registrationRequest->password)
            ->setActivationCode(Uuid::uuid4())
            ->setRoles([UserRoleType::ROLE_USER]);
        $this->em->persist($user);
        $this->em->flush($user);

        $this->expectException(UserExistsException::class);
        $this->expectExceptionCode(409);
        $this->expectExceptionMessage('user already exist');
        $this->securityService->registration($this->registrationRequest);
    }

    public function testResetPassword()
    {
        $request = new ResetPasswordRequest();
        $request->email = 'd@l.er';
        $data = ['email' => $request->email, 'password' => 'password'];
        $user = $this->createUser($data);

        $response = $this->securityService->resetPassword($request);
        $this->assertInstanceOf(ResetPasswordResponse::class, $response);
        $this->assertEquals($request->email, $response->email);
        $this->assertNotEmpty($response->token);
    }

    private function createUser(array $data)
    {
        $user = new User();
        $user
            ->setEmail($data['email'])
            ->setPassword($data['password'])
            ->setActivationCode($data['activationCode'] ?? Uuid::uuid4())
            ->setRoles([UserRoleType::ROLE_USER]);
        $this->em->persist($user);
        $this->em->flush($user);

        return $user;
    }
}